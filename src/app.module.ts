import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { Student } from './students/entities/student.entity';
import { StudentModule } from './students/student.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: '',
      database: 'student_database',
      entities: [Student], // Add your entity classes here
      synchronize: true, // Auto-generate and sync the database schema (in development)
    }),
    StudentModule, // Add the SchoolsModule to the imports array
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}