export interface SchoolInfo {
  id: number;
  name: string;
  address: string;
}
