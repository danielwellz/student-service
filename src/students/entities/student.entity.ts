import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';
import { SchoolInfo } from '../models/school-info.interface';
import { ApiProperty } from '@nestjs/swagger';

@Entity('students')
export class Student {
  @ApiProperty()
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @Column({ name: 'first_name' })
  name: string;

  @ApiProperty()
  @Column({ name: 'last_name' })
  lastName: string;

  @ApiProperty()
  @Column()
  schoolId: number;

  @ApiProperty()
  school?: SchoolInfo;
}
