import {
  Injectable,
  Logger,
  NotFoundException,
  BadRequestException,
} from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Student } from './entities/student.entity';
import { CreateStudentDto } from './dtos/create-student.dto';
import { PartialUpdateStudentDto } from './dtos/update-student.dto';
import axios from 'axios';
import { SchoolInfo } from './models/school-info.interface';

@Injectable()
export class StudentService {
  private readonly logger = new Logger(StudentService.name);

  constructor(
    @InjectRepository(Student) private studentRepository: Repository<Student>,
  ) {}

  async createStudent(createStudentDto: CreateStudentDto): Promise<Student> {
    try {
      const newStudent = this.studentRepository.create(createStudentDto);
      const savedStudent = await this.studentRepository.save(newStudent);
      this.logger.log(`Created student with ID ${savedStudent.id}`);
      return savedStudent;
    } catch (error) {
      this.logger.error(`Failed to create student. Error: ${error.message}`);
      throw new BadRequestException('Failed to create student');
    }
  }

  async updateStudent(
    id: number,
    updateStudentDto: PartialUpdateStudentDto,
  ): Promise<Student> {
    try {
      await this.studentRepository.update(id, updateStudentDto);
      const updatedStudent = await this.getStudentById(id);
      this.logger.log(`Updated student with ID ${id}`);
      return updatedStudent;
    } catch (error) {
      this.logger.error(`Failed to update student. Error: ${error.message}`);
      throw new BadRequestException('Failed to update student');
    }
  }

  async deleteStudent(id: number): Promise<void> {
    try {
      await this.studentRepository.delete(id);
      this.logger.log(`Deleted student with ID ${id}`);
    } catch (error) {
      this.logger.error(`Failed to delete student. Error: ${error.message}`);
      throw new BadRequestException('Failed to delete student');
    }
  }

  async getStudentById(id: number): Promise<Student> {
    try {
      const student = await this.studentRepository.findOne({ where: { id } });
      if (!student) {
        this.logger.error(`Student with ID ${id} not found`);
        throw new NotFoundException(`Student with ID ${id} not found`);
      }
      const schoolId = student.schoolId;
      const school = await this.fetchSchoolById(schoolId);
      student.school = school; // Attach school information to student object
      return student;
    } catch (error) {
      this.logger.error(`Failed to get student by ID. Error: ${error.message}`);
      throw new NotFoundException(`Student with ID ${id} not found`);
    }
  }

  async getStudents(): Promise<Student[]> {
    try {
      const students = await this.studentRepository.find();
      for (const student of students) {
        const schoolId = student.schoolId;
        const school = await this.fetchSchoolById(schoolId);
        student.school = school; // Attach school information to each student object
      }
      return students;
    } catch (error) {
      this.logger.error(`Failed to get students. Error: ${error.message}`);
      throw new BadRequestException('Failed to get students');
    }
  }

  private async fetchSchoolById(schoolId: number): Promise<SchoolInfo> {
    try {
      const response = await axios.get(
        `http://localhost:3000/schools/${schoolId}`,
      );
      return response.data;
    } catch (error) {
      this.logger.error(
        `Error fetching school with ID ${schoolId}: ${error.message}`,
      );
      return null; // Return null or an empty school object on error
    }
  }

  async createRandomStudents(): Promise<Student[]> {
    try {
      const randomStudents: CreateStudentDto[] = [];

      for (let i = 1; i <= 25; i++) {
        randomStudents.push({
          name: `Random First Name ${i}`,
          lastName: `Random Last Name ${i}`,
          schoolId: Math.floor(Math.random() * 10) + 1, // Random schoolId between 1 and 10
        });
      }

      const createdStudents: Student[] = [];

      for (const studentDto of randomStudents) {
        const createdStudent = await this.createStudent(studentDto);
        createdStudents.push(createdStudent);
      }

      return createdStudents;
    } catch (error) {
      this.logger.error(
        `Failed to create random students. Error: ${error.message}`,
      );
      throw new BadRequestException('Failed to create random students');
    }
  }
}
