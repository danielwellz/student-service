import { IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateStudentDto {
  @ApiProperty({
    description: 'First name of the student',
    example: 'John',
    minLength: 2,
    maxLength: 50,
  })
  @IsNotEmpty({ message: 'First name should not be empty' })
  name: string;

  @ApiProperty({
    description: 'Last name of the student',
    example: 'Doe',
    minLength: 2,
    maxLength: 50,
  })
  @IsNotEmpty({ message: 'Last name should not be empty' })
  lastName: string;

  @ApiProperty({
    description: 'ID of the school the student belongs to',
    example: 1,
  })
  @IsNotEmpty({ message: 'School ID should not be empty' })
  schoolId: number;
}
