import { PartialType } from '@nestjs/mapped-types';
import { ApiProperty } from '@nestjs/swagger';
import { IsOptional } from 'class-validator';

export class UpdateStudentDto {
  @ApiProperty({
    description: 'Updated first name of the student',
    example: 'John',
    minLength: 2,
    maxLength: 50,
    required: false,
  })
  @IsOptional()
  name?: string;

  @ApiProperty({
    description: 'Updated last name of the student',
    example: 'Doe',
    minLength: 2,
    maxLength: 50,
    required: false,
  })
  @IsOptional()
  lastName?: string;

  @ApiProperty({
    description: 'Updated ID of the school the student belongs to',
    example: 1,
    required: false,
  })
  @IsOptional()
  schoolId?: number;
}

export class PartialUpdateStudentDto extends PartialType(UpdateStudentDto) {}
