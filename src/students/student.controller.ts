import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Delete,
  Param,
  HttpStatus,
  NotFoundException,
  BadRequestException,
  Logger,
} from '@nestjs/common';
import { StudentService } from './student.service';
import { Student } from './entities/student.entity';
import { CreateStudentDto } from './dtos/create-student.dto';
import { UpdateStudentDto } from './dtos/update-student.dto';
import {
  ApiTags,
  ApiResponse,
  ApiBadRequestResponse,
  ApiNotFoundResponse,
} from '@nestjs/swagger';

@ApiTags('students')
@Controller('students')
export class StudentController {
  private readonly logger = new Logger(StudentController.name);

  constructor(private readonly studentService: StudentService) {}

  @Get()
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Get all students',
    type: [Student],
  })
  async getStudents(): Promise<Student[]> {
    this.logger.verbose('Fetching all students');
    return this.studentService.getStudents();
  }

  @Post('create')
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Create a new student',
    type: Student,
  })
  async createStudent(
    @Body() createStudentDto: CreateStudentDto,
  ): Promise<Student> {
    try {
      this.logger.debug('Creating new student');
      return await this.studentService.createStudent(createStudentDto);
    } catch (error) {
      this.logger.error('Failed to create student');
      throw new BadRequestException('Failed to create student');
    }
  }

  @Get(':id')
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Get student by ID',
    type: Student,
  })
  @ApiNotFoundResponse({ description: 'Student not found' })
  async getStudentById(@Param('id') id: number): Promise<Student> {
    try {
      this.logger.debug(`Fetching student with ID ${id}`);
      return await this.studentService.getStudentById(id);
    } catch (error) {
      this.logger.error(`Student with ID ${id} not found`);
      throw new NotFoundException(`Student with ID ${id} not found`);
    }
  }

  @Patch('update/:id')
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Update student by ID',
    type: Student,
  })
  @ApiBadRequestResponse({ description: 'Invalid data provided' })
  @ApiNotFoundResponse({ description: 'Student not found' })
  async updateStudent(
    @Param('id') id: number,
    @Body() updateStudentDto: UpdateStudentDto,
  ): Promise<Student> {
    try {
      this.logger.debug(`Updating student with ID ${id}`);
      const updatedStudent = await this.studentService.updateStudent(
        id,
        updateStudentDto,
      );
      if (!updatedStudent) {
        throw new NotFoundException(`Student with ID ${id} not found`);
      }
      return updatedStudent;
    } catch (error) {
      this.logger.error('Failed to update student');
      throw new BadRequestException('Failed to update student');
    }
  }

  @Delete('delete/:id')
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'Delete student by ID',
  })
  @ApiNotFoundResponse({ description: 'Student not found' })
  async deleteStudent(@Param('id') id: number): Promise<void> {
    try {
      this.logger.debug(`Deleting student with ID ${id}`);
      await this.studentService.deleteStudent(id);
    } catch (error) {
      this.logger.error('Failed to delete student');
      throw new BadRequestException('Failed to delete student');
    }
  }

  @Post('create-sample')
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'Create random students',
    type: [Student],
  })
  async createRandomStudents(): Promise<Student[]> {
    try {
      this.logger.verbose('Creating 25 random students');
      return this.studentService.createRandomStudents();
    } catch (error) {
      this.logger.error('Failed to create random students');
      throw new BadRequestException('Failed to create random students');
    }
  }
}
